#!/bin/bash 
set -x

sleep 6 # initial bootup sleep

cd /root//orangepi_sht10
cnt=0	## let system reboot when something is wrong
while [  $cnt -lt 100 ]; do
	echo The counter is $cnt
	python3 SHT10_MQTT.py mqttConfig.json > /dev/null
	sleep 3
	cnt=$((cnt+1));
done

echo system reboot 
shutdown -r now


