#!/usr/bin/env python3
## publishing sensor data to MQTT broker

import os
import time
import sys
#import sht1x_sensor
import paho.mqtt.client as mqtt
import json
import random
import queue
from datetime import datetime
import logging
import OPi.GPIO as GPIO
from pi_sht1x import SHT1x

# kind of request message
MSG_CTRL=1
MSG_PARA=2
MSG_ERR=3

DATA_PIN = "PA12"
SCK_PIN = "PA11"

class SHT10Sensor:
    def __init__(self, logger=None):
        GPIO.setwarnings(False)
        self.sensor = SHT1x(DATA_PIN, SCK_PIN, gpio_mode=GPIO.SUNXI, logger=logger, crc_check=False)

    def read_values(self):
        temp = self.sensor.read_temperature()
        humidity = self.sensor.read_humidity(temp)
        return (temp, humidity)

def generate_topic(service_code, node_id):
    topic = 'IoTF1/' + service_code + '/' + node_id + '/stat/_cur'
    return topic

# make topic for remote call
def generate_rcall_filter(service_code, node_id):
    topic = 'IoTF1/' + service_code + '/' + node_id + '/rcall/#'
    return topic

def generate_client_id(client_prefix, node_id):
    client_id = client_prefix + '_' + node_id + '_' + str(random.randint(100, 999))
    return client_id

def load_json_file(path):
    with open(path) as data_file:
        return json.load(data_file)

def save_json_file(path, data):
    with open(path, 'w') as data_file:
        return json.dump(data, data_file, indent=2)

if len(sys.argv) != 2:
    print(sys.argv[0], "mqttConfig.json")
    sys.exit(0)

mqtt_config = load_json_file(sys.argv[1])
node_id = mqtt_config['nodeId']
report_interval = mqtt_config['reportInterval']
if report_interval < 1 or report_interval > 60:
    print(sys.argv[0], "out of range: reportInterval in {}", sys.argv[1])
    sys.exit(0)

topic = generate_topic(mqtt_config['serviceCode'], node_id)
client_id = generate_client_id(mqtt_config['clientPrefix'], node_id)

sht = SHT10Sensor(logging)

ACCESS_TOKEN = 'IOT_DEMO_TOKEN'
ipc_queue = queue.Queue()
next_reading = time.time()

def handle_mqtt_msg(msg):
    global ipc_queue
    parsed_topic = msg.topic.split('/')
    payload = msg.payload.decode('utf-8')
    data = json.loads(payload)
    if parsed_topic[3] == 'rcall':
        if parsed_topic[4] == '_ctrl':
            ipc_queue.put((MSG_CTRL, data))
        elif parsed_topic[4] == '_para':
            ipc_queue.put((MSG_PARA, data))
        else:
            print("unknown topic: {}".format(msg.topic))
    else:
        print("unknown topic: {}".format(msg.topic))

def on_message(mqttc, obj, msg):
    """ topic example>
    - control topic: IoTF1/1/1001/rcall/_ctrl/123456
    """
    #print("on message: " + msg.topic+" "+str(msg.qos)+" payload: "+str(msg.payload))
    #print("msg.payload --> type:{} text: {} len: {}".format(type(msg.payload), msg.payload, len(msg.payload)))
    try:
        handle_mqtt_msg(msg)
    except ValueError as e:
        print("no json text: {}".format(msg))
    except:
        print("Unexpected error: {}".format(sys.exc_info()[0]))
        ipc_queue.put((MSG_ERR, None))

def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("connected ok")
    else:
        print("connected failed")
        ipc_queue.put((MSG_ERR, None))

def on_disconnect(client, userdata, r):
    print("disconnect: ", r)
    ipc_queue.put((MSG_ERR, None))

def process_chat(data):
    print('chat data: ', data)

def process_ctrl(data):
    print('ctrl data: ', data)

def process_param(data):
    print('param data: ', data)

def check_queue_data():
    try:
        item = ipc_queue.get(True, 0.5)
    except queue.Empty:
        return True
    try:
        if item[0] == MSG_CTRL:
            process_ctrl(item[1])
        elif item[0] == MSG_PARA:
            process_param(item[1])
        elif item[0] == MSG_ERR:
            print("error detected")
            return False
        return True
    except:
        print("error: {}".format(sys.exc_info()))

print("MQTT client_id={} with interval={}".format(client_id, report_interval))
client = mqtt.Client(client_id)
client.on_message = on_message
client.on_connect = on_connect
client.on_disconnect = on_disconnect

# Set access token
client.username_pw_set(ACCESS_TOKEN)

print("MQTT mqttLocalHost={} mqttLocalPort={}".format(mqtt_config['mqttLocalHost'], mqtt_config['mqttLocalPort']))
# Connect to MQTT broker using mqttLocalHost and port and 60 seconds keepalive interval
client.connect(mqtt_config['mqttLocalHost'], mqtt_config['mqttLocalPort'], 60)

temp, humi = (0, 0)
client.loop_start()

topics = []
topics.append((generate_rcall_filter(mqtt_config['serviceCode'], node_id), 0))
print("subscribe {}".format(topics))
client.subscribe(topics, 0)

keep_going = True
try:
    while keep_going:
        try:
            temp,humi = sht.read_values()
            #temp,humi = (12, 34)
        except Exception as ex:
            print("exception: {0}".format(ex))
            break

        sensor_data = {
          "tm" : int(time.time()),
          "temp" : {"unit": "C", "val": temp},
          "humi" : {"unit": "%", "val": humi}
        }
        data_in_json_format = json.dumps(sensor_data)
        print("publish: {:s} {:s}".format(topic, data_in_json_format))
        client.publish(topic, data_in_json_format, 1)
        next_reading += report_interval
        sleep_time = next_reading-time.time()
        while sleep_time > 0:
            keep_going = check_queue_data()
            if not keep_going:
                break
            sleep_time = next_reading-time.time()
            #print('check')
        if not keep_going:
            keep_going = check_queue_data()
        time.sleep(1)
except KeyboardInterrupt:
    pass

client.loop_stop()
client.disconnect()
