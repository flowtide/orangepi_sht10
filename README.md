Publishing SHT10 sensor data to MQTT server.
-----------------
Running platform is Orange PI Zero.

Program run setup
---------------

```bash

# Install  OPi.GPIO python3 package
sudo pip3 install OPi.GPIO

# Install SHT10 python3 package
sudo pip3 install pi-sht1x

# install MQTT client library
sudo pip3 install paho-mqtt
```

Modify OPi.GPIO package source code
---------------
```bash
### Uncomment channel configuration check statements.
# diff /usr/local/lib/python3.6/dist-packages/OPi/GPIO.py /usr/local/lib/python3.6/dist-packages/OPi/GPIO.py.org   
352,353c352,353
<         #if channel in _exports:
<         #    raise RuntimeError("Channel {0} is already configured".format(channel))
---
>         if channel in _exports:
>             raise RuntimeError("Channel {0} is already configured".format(channel))
```

Run program
---------------

```bash

# Edit mqttConfig.json file according to your environment

# run program
python3 SHT10_MQTT.py mqttConfig.json
```
